@extends("master")

@section("content")


    <style>


        .container {
            padding: 0px 20px;
            max-width: 800px;
            margin: 0 auto;
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .contact-form {
            width: 90%;
            background-color: #c1c0c0;
            box-shadow: 0 8px 12px rgba(0,0,0,0.1);
            border-radius: 15px;
            padding: 40px;
            transition: all 0.3s;
            text-align: center;

        }

        .contact-form:hover {
            box-shadow: 0 12px 18px rgba(0,0,0,0.15);
            transform: translateY(-10px);
        }

        label {
            font-size: 18px;
            font-weight: bold;
            color: #515151;
            margin-bottom: 10px;
            display: block;
        }

        textarea, input {
            border: 1px solid #D0D0D0;
            border-radius: 5px;
            padding: 10px;
            font-size: 16px;
            outline: none;
            transition: all 0.3s;
        }

        input:focus, textarea:focus {
            border: 1px solid #D9886A;
            box-shadow: 0 0 5px rgba(217,136,106,0.1);
        }

        textarea {
            min-height: 150px;
            resize: none;
        }

        button {
            background-color: #D9886A;
            color: white;
            text-transform: uppercase;
            font-weight: bold;
            cursor: pointer;
            padding: 10px 20px;
            border-radius: 25px;
            border: none;
            font-size: 18px;
            transition: all 0.3s;
            float: left;
            max-width: 200px;
            text-align: center;
            margin-top: 10px;
        }

        button:hover {
            background-color: #BF6F4C;
        }

        @media (max-width: 600px) {
            header {
                height: 70px;
                font-size: 24px;
            }

            .contact-form {
                padding: 25px;
            }

            label, input, textarea {
                font-size: 14px;
            }

            button {
                font-size: 16px;
            }
        }


        h1 {
            color: #7787A6;
            font-size: 36px;
            margin-bottom: 30px;
            text-align: center;
        }
    </style>

<body>
<div class="container">
    <h1>تماس با ما</h1>
    <form class="contact-form">
        <label for="name">نام</label>
        <input type="text" id="name">

        <label for="email">ایمیل</label>
        <input type="email" id="email">

        <label for="message">پیام</label>
        <textarea id="message"></textarea>
    </form>
    <button type="submit">ارسال پیام</button>
</div>
</body>


@endsection

