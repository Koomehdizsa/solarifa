
@extends("master")

@section("content")




    <div class="main-login-container">
        <div class="login-container">
            <h1>خوش آمدید</h1>

            <div class="email-field">
                <label for="email">ایمیل</label>
                <input type="email" name="email" id="email" required>
            </div>

            <div class="password-field">
                <label for="password">پسوورد</label>
                <input type="password" name="password" id="password" required>
            </div>

            <form action="/home" method="get">
                <button type="submit">ورود</button>
            </form>
            <div class="forgot-password">
                <a href="#">فراموشی رمز عبور</a>
            </div>

            <form action="/register" method="get">
                <button type="submit" class="signup-btn">ثبت نام</button>
            </form>
        </div>
    </div>

    <style>

        * {
            box-sizing: border-box;
        }

        .main-login-container{
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 20vh;
            background-color: #222;
            color: #fff;
        }

        .login-container {
            width: 90%;
            max-width: 500px;
            padding: 20px;
            background-color: #333;
            border-radius: 5px;
            text-align: center;
            box-shadow: 0px 0px 10px rgba(0,0,0,0.4);
        }

        .email-field,
        .password-field {
            margin-bottom: 10px;
        }

        label {
            display: block;
            margin-bottom: 5px;
        }

        input {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 30px;
        }


        button {
            width: 100%;
            padding: 10px;
            border: none;
            background-color: #d9886a;
            color: #fff;
            font-size: 16px;
            cursor: pointer;
            transition: background-color 0.3s;
            border-radius: 30px; / Make the buttons rounded /
        }

        button:hover {
            background-color: #d9886a;
        }

        .forgot-password {
            margin: 20px 0;
        }

        .forgot-password a {
            color: #aaa;
            text-decoration: none;
            transition: color 0.3s;
        }



        .forgot-password a:hover {
            color: #ccc;
        }

        .signup-btn {
            background-color: #444;
        }

        .signup-btn:hover {
            background-color: #666;
        }

        @media (max-width: 480px) {
            input,
            button {
                font-size: 14px;
            }
        }

    </style>

@endsection
