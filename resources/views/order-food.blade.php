@extends("master")

@section("content")


    <style>


        header {
            background-color: #7787A6;
            height: 70px;
            display: flex;
            justify-content: center;
            align-items: center;
            color: white;
            font-size: 24px;
        }

        .container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            padding: 20px;
        }
        .card {
            width: 250px;
            background-color: #7787a6;
            margin: 10px;
            box-shadow: 0 4px 6px rgba(0,0,0,0.1);
            overflow: hidden;
            border-radius: 15px;
            display: flex;
            flex-direction: column;
            align-items: center;
            transition: all 0.3s;
        }

        img {
            width: 100%;
            height: auto;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
        }

        .details {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            padding: 15px;
        }

        .food-name {
            font-size: 18px;
            font-weight: bold;
            color: #434343;
            margin-bottom: 10px;
            text-align: center;
        }

        .btn {
            background-color: #D9886A;
            color: white;
            text-transform: uppercase;
            font-weight: bold;
            cursor: pointer;
            padding: 8px 20px;
            border-radius: 25px;
            border: none;
            transition: all 0.3s;
        }

        .card:hover .btn {
            background-color: #BF6F4C;
        }

        .card:hover {
            box-shadow: 0 8px 12px rgba(0,0,0,0.15);
            transform: translateY(-10px);
        }

        @media (max-width: 600px) {
            .card {
                width: 100%;
                margin: 10px 0;
            }
        }

        h1 {
            color: #7787A6;
            font-size: 36px;
            margin-bottom: 30px;
        }
    </style>

    <div class="container">
        <h1>سفارش غذا</h1>
        <div class="card">
            <img src="/food.jpg" alt="غذا 1">
            <div class="details">
                <div class="food-name">غذای 1</div>
                <form action="/basket" method="get">
                    <button type="submit" class="btn">سفارش</button>
                </form>
            </div>
        </div>
        <div class="card">
            <img src="/food.jpg" alt="غذا 2">
            <div class="details">
                <div class="food-name">غذای 2</div>
                <form action="/basket" method="get">
                    <button type="submit" class="btn">سفارش</button>
                </form>
            </div>
        </div>
        <div class="card">
            <img src="/food.jpg" alt="غذا 3">
            <div class="details">
                <div class="food-name">غذای 3</div>
                <form action="/basket" method="get">
                    <button type="submit" class="btn">سفارش</button>
                </form>
            </div>
        </div>
        <div class="card">
            <img src="/food.jpg" alt="غذا 4">
            <div class="details">
                <div class="food-name">غذای 4</div>
                <form action="/basket" method="get">
                    <button type="submit" class="btn">سفارش</button>
                </form>
            </div>
        </div>
    </div>


@endsection
