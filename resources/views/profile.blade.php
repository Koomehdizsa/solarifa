
@extends("master")

@section("content")

    <div class="main-login-container">
        <!-- Profile Section -->
        <div class="profile-picture"></div>
        <section class="profile-section">

            <!-- Profile Picture -->


            <!-- Information Section -->
            <div class="info-section">
                <h1>صفا مهدی</h1>
                <p>safamehdizadegan@gmail.com</p>
                <p>تعداد سفارشات : ۱۰ عدد</p>

                <!-- Social Media Links -->

                <!-- Example: `<a href="#" target="_blank"><i class="fab fa-twitter"></i></a>` -->
                <!-- Add your own social media icons and links here -->
            </div>


        </section>

        <form action="/login" method="get">
            <button type="submit">خروج</button>
        </form>
    </div>


    <style>
        button {
            width: 100%;
            padding: 10px;
            border: none;
            background-color: #d9886a;
            color: #fff;
            font-size: 16px;
            cursor: pointer;
            transition: background-color 0.3s;
            border-radius: 30px; / Make the buttons rounded /
        }

        button:hover {
            background-color: #d9886a;
        }

        .main-login-container{
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        /* Profile section styles */
        .profile-section {
            display: flex;
            align-items: center;
            justify-content:center ;
            margin-top :50px ;
            flex-direction: row-reverse;
        }

        /* Profile picture styles */
        .profile-picture {
            width:200px;
            height:200px;
            background-image:url('/profile.avif');
            background-size :cover;
            border-radius :50% ;
            box-shadow :0px .5rem .5rem rgba(0 ,0 ,0 ,.3) ;

        }


        .info-section {
            text-align:center ;
        }

        /* Information section styles */
        .info-section h1,
        .info-section p {
            margin-top :0 ;
        }

        .info-section h1 {
            font-size: 2rem;
            margin-bottom: .5rem;
            color: #c1c0c0;

        }

        .info-section p {
            font-size: 1.2rem;
            color: #c1c0c0;

        }
    </style>

@endsection
