
@extends("master")

@section("content")


    <style>

        .container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            height: 30vh;
            width: 80%;
            margin: auto;
        }


        .option {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100px;
            height: 100px;
            margin: 20px;
            border-radius: 50%;
            text-transform: uppercase;
            font-weight: bold;
            cursor: pointer;
        }

        .option:hover {
            transform: scale(1.1);
            transition: all 0.3s ease;
        }

        .option-one {
            background-color: #7787A6;
            color: white;
        }

        .option-two {
            background-color: #D9886A;
            color: white;
        }

        @media screen and (max-width: 480px) {
            .option {
                width: 80px;
                height: 80px;
                margin: 10px;
                font-size: 14px;
            }
        }


        .responsive {
            width: 85%;
            height: auto;
            border-radius: 20%;
        }

        h1 {
            color: #7787A6;
            font-size: 36px;
            text-align: center;
        }

        .items{
            padding-top: 70px;
            display: flex;
        }
    </style>
    <div class="container">
        <h1>خانه</h1>
        <img src="/cafe.webp" alt="Nature" class="responsive">

        <div class="items">
            <a href="/music">
                <div class="option option-one">
                    موزیک
                </div>
            </a>
            <a href="/order-food">
                <div class="option option-two">
                    غذا
                </div>
            </a>
            <div class="option option-one">
                گارسون
            </div>
        </div>
    </div>

@endsection
