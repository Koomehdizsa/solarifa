<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{asset('assets-dash/font/IRANSans4/WebFonts/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets-dash/font/IRANSansNum/style.css')}}">

    <title>Select Options Page</title>
    <style>
        body {
            background-color: #222;
            margin: 0;
            font-family: sans-serif;
        }





        .background {
            background-image: url(/back.png);
            background-repeat: repeat;
            background-size: 400px 400px;
            bottom: 0;
            left: 0;
            right: 0;
            position: absolute;
            top: 0;
            opacity: 0.05;
            z-index: -1;
        }

        footer{
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            /*background: #222;*/
            height: auto;
            width: 100vw;
            padding-top: 40px;
            color: #fff;
            text-align: right;
        }

        .footer-span{
            color: #c1c0c0;
        }

        .footer-div{
            padding-right: 10px;
        }

        .header {
            display: flex;
            text-align: right;
            overflow: hidden;
            /* background-color: #f1f1f1; */
            padding: 20px 10px;
            flex-direction: column;
            align-items: center;
        }

        /* Style the header links */
        .header a {
            float: left;
            color: black;
            text-align: center;
            padding: 12px;
            text-decoration: none;
            font-size: 18px;
            line-height: 25px;
            border-radius: 4px;
        }

        /* Style the logo link (notice that we set the same value of line-height and font-size to prevent the header to increase when the font gets bigger */
        .header a.logo {
            font-size: 25px;
            font-weight: bold;
        }

        /*!* Change the background color on mouse-over *!*/
        /*.header a:hover {*/
        /*    background-color: #ddd;*/
        /*    color: black;*/
        /*}*/

        /*!* Style the active/current link*!*/
        /*.header a.active {*/
        /*    background-color: dodgerblue;*/
        /*    color: white;*/
        /*}*/

        /* Float the link section to the right */
        .header-right {
            float: right;
        }

        /* Add media queries for responsiveness - when the screen is 500px wide or less, stack the links on top of each other */
        @media screen and (max-width: 500px) {
            .header a {
                color: #c1c0c0;
                float: none;
                display: block;
                text-align: left;
            }
            .header-right {
                display: flex;
                float: none;
            }
        }

        .top-header-items{
            display: flex;
            justify-content: space-between;
        }

        .header-item {
            margin-left: 25px;
            margin-right: 25px;
            border-radius :50% ;
            box-shadow :0px .5rem .5rem rgba(0 ,0 ,0 ,.3) ;
        }
    </style>
</head>
<body>

<div class="background"></div>
<div class="header">
    <div class="top-header-items">
        <a href="/profile" class="profile-icon header-item">پروفایل</a>
        <a href="/home" class="logo header-item">سولاریفا</a>
        <a href="/basket" class="profile-icon header-item">سبد</a>
    </div>
    <div class="header-right">
        <a class="active" href="/home">خانه</a>
        <a href="/contact-us">تماس با ما</a>
        <a href="/about-us">درباره ما</a>
    </div>
</div>

@yield('content')



</div>

{{--<footer>--}}
{{--    <div class="footer-div">--}}
{{--        <span class="footer-span">تمامی حقوق این اپ برای صفا مهدی‌زادگان می‌باشد - ۱۴۰۲</span>--}}
{{--    </div>--}}
{{--</footer>--}}

</body>

</html>
