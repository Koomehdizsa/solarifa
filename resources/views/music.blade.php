@extends("master")

@section("content")

    <style>

        .container {
            max-width: 800px;
            margin: 0 auto;

            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .image-container {
            width: 80%;
            height: 0;
            padding-top: 100%;
            background-image: url('/music.jpg');
            background-size: cover;
            background-position: center;
            border-radius: 10px;
            box-shadow: 0px 20px 50px rgba(0, 0, 0, 0.5);
            margin-bottom: 50px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            position: relative;
        }

        .image-container h1 {
            color: #fff;
            font-size: 36px;
            text-align: center;
            position: absolute;
            bottom: 20px;
            left: 0;
            right: 0;
            margin: 0;
        }

        .buttons-container {
            display: flex;
            justify-content: space-around;
            width: 100%;
            position: absolute;
            bottom: -68px;
            left: 0;
            right: 0;
        }

        .button {
            width: 80px;
            height: 40px;
            background-color: #D9886A;
            color: #fff;
            font-size: 16px;
            border-radius: 20px;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;
            transition: all 0.3s ease-in-out;

        }

        .button:hover {
            background-color: #fff;
            color: #D9886A;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
        }

        .list-container {
            background-color: #c1c0c0;
            width: 80%;
            padding: 20px;
            box-shadow: 0px 20px 50px rgba(0, 0, 0, 0.5);
            border-radius: 10px;
            display: flex;
            flex-direction: column;
            align-items: stretch;
            justify-content: space-between;
            margin-top: 40px;
            align-content: space-between;

        }

        .list-item {
            font-size: 22px;
            color: #444;
            margin-bottom: 10px;
            text-align: center;
            display: flex;
            justify-content: space-around;
            align-items: stretch;
            flex-wrap: nowrap;
            flex-direction: row-reverse;
            align-content: stretch;

        }

        @media screen and (max-width: 600px) {
            .image-container h1 {
                font-size: 24px;
                bottom: 10px;
            }

            .button {
                width: 70px;
                height: 35px;
                font-size: 14px;
            }

            .list-item {
                font-size: 22px;
                color: #444;
                margin-bottom: 10px;
                text-align: center;

                display: flex;
                flex-direction: row-reverse;
            }
        }

        h1 {
            color: #7787A6;
            font-size: 36px;
            text-align: center;
        }
    </style>

    <div class="container">
        <h1>پخش موزیک</h1>
        <div class="image-container">
            <!-- Add your image here -->
            <h1>تیتر موزیک</h1>
            <div class="buttons-container">
                <button class="button">قبلی</button>
                <button class="button">پخش</button>
                <button class="button">بعدی</button>
            </div>
        </div>
        <div class="list-container">
            <!-- Add your list items here -->
            <div class="list-item">
                <button class="button">پخش</button>
                عنوان ۱</div>
            <div class="list-item">
                <button class="button">پخش</button>
                عنوان ۲</div>
            <div class="list-item">
                <button class="button">پخش</button>
                عنوان ۳</div>
        </div>
    </div>


@endsection
