@extends("master")

@section("content")



    <style>


        header {
            background-color: #7787A6;
            height: 90px;
            display: flex;
            justify-content: center;
            align-items: center;
            color: white;
            font-size: 30px;
            box-shadow: 0 4px 6px rgba(0,0,0,0.1);
        }

        .container {
            padding: 20px;
            max-width: 800px;
            margin: 0 auto;
        }

        table {
            width: 100%;
            background-color: #c1c0c0;
            box-shadow: 0 4px 6px rgba(0,0,0,0.1);
            border-collapse: collapse;
            border-radius: 15px;
            overflow: hidden;
        }

        th, td {
            padding: 15px;
            border-bottom: 1px solid #E0E0E0;
        }

        th {
            background-color: #c1c0c0;
            /*color: white;*/
            font-weight: bold;
            text-transform: uppercase;
        }

        td:first-child, th:first-child {
            border-top-left-radius: 15px;
        }

        td:last-child, th:last-child {
            border-top-right-radius: 15px;
            text-align: center;
        }

        .delete-btn {
            background-color: #D9886A;
            color: white;
            text-transform: uppercase;
            font-weight: bold;
            cursor: pointer;
            padding: 8px 20px;
            border-radius: 25px;
            border: none;
            transition: all 0.3s;
        }

        .delete-btn:hover {
            background-color: #BF6F4C;
        }

        tfoot td {
            background-color: #c1c0c0;
            /*color: #fffefe;*/
            font-weight: bold;
            padding: 20px;
            border-bottom: none;
        }

        span.price {
            float: right;
            margin-left: 10px;
        }

        @media (max-width: 600px) {
            th, td {
                padding: 10px;
                font-size: 14px;
            }

            .delete-btn {
                padding: 6px 10px;
            }
        }


        .btn {
            background-color: #D9886A;
            color: white;
            text-transform: uppercase;
            font-weight: bold;
            cursor: pointer;
            padding: 18px 46px;
            border-radius: 25px;
            border: none;
            transition: all 0.3s;
            font-size: larger;
        }

        .order-button{
            align-items: flex-start;
            padding-top: 40px;
            display: flex;
            justify-content: space-around;
        }

        h1 {
            color: #7787A6;
            font-size: 36px;
            margin-bottom: 30px;
            text-align: center;
        }


        .popup {
            position: relative;
            display: inline-block;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* The actual popup */
        .popup .popuptext {
            visibility: hidden;
            width: 160px;
            background-color: #555;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 8px 0;
            position: absolute;
            z-index: 1;
            bottom: 125%;
            left: 50%;
            margin-left: -80px;
        }

        /* Popup arrow */
        .popup .popuptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }

        /* Toggle this class - hide and show the popup */
        .popup .show {
            visibility: visible;
            -webkit-animation: fadeIn 1s;
            animation: fadeIn 1s;
        }

        /* Add animation (fade in the popup) */
        @-webkit-keyframes fadeIn {
            from {opacity: 0;}
            to {opacity: 1;}
        }

        @keyframes fadeIn {
            from {opacity: 0;}
            to {opacity:1 ;}
        }
    </style>

    <div class="container">
        <h1>سبد خرید</h1>
        <table>
            <thead>
            <tr>
                <th>عنوان محصول</th>
                <th>قیمت</th>
                <th>حذف</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>محصول 1</td>
                <td>10000 تومان</td>
                <td><button class="delete-btn">حذف</button></td>
            </tr>
            <tr>
                <td>محصول 2</td>
                <td>20000 تومان</td>
                <td><button class="delete-btn">حذف</button></td>
            </tr>
            <tr>
                <td>محصول 3</td>
                <td>15000 تومان</td>
                <td><button class="delete-btn">حذف</button></td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td>مجموع</td>
                <td colspan="2">45000 تومان<span class="price"></span></td>
            </tr>
            </tfoot>
        </table>

        <div class="order-button">
{{--            <button class="btn">سفارش</button>--}}

            <button class="popup btn" onclick="myFunction()">سفارش
                <span class="popuptext" id="myPopup">سفارش شما ثبت شد</span>
            </button>
        </div>


    </div>


    <script>
        // When the user clicks on div, open the popup
        function myFunction() {
            var popup = document.getElementById("myPopup");
            popup.classList.toggle("show");
        }
    </script>

@endsection

